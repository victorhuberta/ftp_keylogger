void protectThisProcess();
BOOL isUserAdmin();
void runMyselfAsAdminAndExit();
BOOL isProcessRunning(char processName[]);
void alwaysLaunchOnStartup();
